const {Router} = require('express')
const {getUsers, deleteUser, getUser, updateUser, createUser} = require("../controllers/users-controller");
const UserRouter = Router()
/**
 * @swagger
 * /users/:
 *   get:
 *     summary: Retrieve all users
 *     description: Get an array of all users
 *     tags:
 *       - Users
 *     security:
 *       - token: []
 *     responses:
 *       200:
 *         description: All users array.
 */
UserRouter.get('/', getUsers)
/**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Retrieve a user by id
 *     tags:
 *       - Users
 *     security:
 *       - token: []
 *     responses:
 *         200:
 *             description: User Object.
 */
UserRouter.get('/:id', getUser)
/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     summary: Delete a user by id
 *     tags:
 *       - Users
 *     security:
 *       - token: []
 *     responses:
 *         200:
 *             description: Message of success.
 */
UserRouter.delete('/:id', deleteUser)
/**
 * @swagger
 * /users/{id}:
 *   put:
 *     summary: Update a user by id
 *     tags:
 *       - Users
 *     security:
 *       - token: []
 *     responses:
 *         200:
 *             description: The updated user.
 */
UserRouter.put('/:id', updateUser)
/**
 * @swagger
 * /users/:
 *   post:
 *     summary: Create a user
 *     tags:
 *       - Users
 *     security:
 *       - token: []
 *     responses:
 *         200:
 *             description: The created user.
 */
UserRouter.post('/', createUser)

module.exports = UserRouter