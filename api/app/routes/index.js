const { Router } = require('express')
const router = Router()

const ProductRouter = require('./products-router')
const UserRouter = require('./users-router')
const {login, logout} = require("../controllers/users-controller");

/**
 * @swagger
 * /login:
 *   post:
 *     summary: Authenticate the user with email and password
 *     description: Authentification of the user and return JWT token
 *     tags:
 *       - Authentication
 *     requestBody:
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 email:
 *                   type: string
 *                   description: Email of the user
 *                   example: test@api.fr
 *                   required: true
 *                   format: email
 *                 password:
 *                   type: string
 *                   description: Password of the user
 *                   example: pswd
 *                   format: password
 *                   required: true
 *     responses:
 *       200:
 *         description: Object with user id, name and generated token.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  _id:
 *                    type: ObjectId
 *                    example: 62188d0b2e6cbqsdz35b0f5c33
 *                    description: Id of the user
 *                    required: true
 *                  name:
 *                      type: string
 *                      example: test
 *                      description: Name of the user
 *                      required: true
 *                  token:
 *                      type: string
 *                      example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMTg4ZDBiMmU2Y2I4YzM1YjBmNWMzMyIsIm5hbWUiOiJUZXN0IEFwaSIsImVtYWlsIjoidGVzdEBhcGkuZnIiLqsdqzdqsCJpYXQiOjE2NDU3Nzc1NjQsImV4cCI6MTY0NTc4MTE2NH0.23sv4HVoqZmkrM2FZ6V_cvd28e7vUlnobQThuaKlHX8
 *                      description: Token of the user
 *                      required: true
 */
router.post('/login',login);

/**
 * @swagger
 * /logout:
 *   post:
 *     summary: Lougout the user and delete the token
 *     description: Lougout the user and delete the token
 *     tags:
 *       - Authentication
 *     requestBody:
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: ObjectId
 *                   description: Id of the user to logout
 *                   example: 62188d0b2e6cb8c35b0f5c33
 *                   required: true
 *     responses:
 *       200:
 *         description: Status Message.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  message:
 *                    type: string
 *                    example: Logout successful
 *                    description: Message of the logout
 *                    required: true
 */
router.post('/logout',logout)
router.use('/users', UserRouter)
router.use('/products',ProductRouter)

module.exports = router