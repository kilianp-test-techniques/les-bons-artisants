const {Router} = require('express')
const {getProducts,getProduct,deleteProduct,createProduct,updateProduct} = require('../controllers/products-controller')
const ProductRouter = Router()

/**
 * @swagger
 * /products:
 *   get:
 *     summary: Get all products
 *     tags:
 *       - Products
 *     security:
 *       - token: []
 *     responses:
 *       200:
 *         description: An array of products
 */
ProductRouter.get('/', getProducts)
/**
 * @swagger
 * /products/{id}:
 *   get:
 *     summary: Get a product by id
 *     tags:
 *       - Products
 *     security:
 *       - token: []
 *     responses:
 *       200:
 *         description: A product
 */
ProductRouter.get('/:id', getProduct)
/**
 * @swagger
 * /products/{id}:
 *   delete:
 *     summary: Delete a product by id
 *     tags:
 *       - Products
 *     security:
 *       - token: []
 *     responses:
 *       200:
 *         description: Message of success
 */
ProductRouter.delete('/:id', deleteProduct)
/**
 * @swagger
 * /products:
 *   post:
 *     summary: Create a product
 *     tags:
 *       - Products
 *     security:
 *       - token: []
 *     responses:
 *       200:
 *          description: The updated product
 */
ProductRouter.put('/:id', updateProduct)
/**
 * @swagger
 * /products:
 *   post:
 *     summary: Create a product
 *     tags:
 *       - Products
 *     security:
 *       - token: []
 *     responses:
 *       200:
 *         description: The created product
 */
ProductRouter.post('/', createProduct)

module.exports = ProductRouter