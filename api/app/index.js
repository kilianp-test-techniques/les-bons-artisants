const express = require('express')
const helmet = require('helmet') // Enforces good security practices
const cors = require('cors') // CORS support for API
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const {auth} = require('./middlewares/authentication')
const {errorsHandler} = require('./middlewares/errors-handler')
const app = express()
const morgan = require('morgan')
const router = require('./routes')
const mongodb     = require('./db/mongo');
const fs = require('fs');
const Users = require('./models/users');
const Products = require('./models/products');
let users = JSON.parse(fs.readFileSync('./app/db/users.json'));
let products = JSON.parse(fs.readFileSync('./app/db/products.json'));
const swaggerJsdoc = require("swagger-jsdoc")
const swaggerUi = require("swagger-ui-express");
const initDb = async () => {

    products.forEach((product) => {
        delete product._id;
        Products.find(product).count().then((res) => {
                res === 0 ? Products.create(product):null;
        })
    })

    users.forEach((user) => {
        Users.find(user).count().then((res) => {
                res === 0 ? Users.create(user):null;
        })
    })

    mongodb.initClientDbConnection();
}

initDb()
const swaggerOptions = {
    swaggerDefinition: {
        openapi: "3.0.0",
        info: {
            version: "1.0.0",
            title: "REST API FOR TECHNICAL TEST 'LES BONS ARTISANTS'",
            description: "To Authenticate you will need a token that you can find when using route '/login'.",
            contact: {
                name: "Kilian PICHARD",
                email: "perso@kilianp.fr",
                url: "http://kilianp.fr"
            },
        },
        servers: [
            {
                url: "http://localhost:3001/api/",
                description: "Development"
            }
        ],
        components: {
            securitySchemes: {
                token: {
                    type: "http",
                    scheme: "Bearer",
                    value: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMTg5ZDUzYTBmNzVhMGVlNDdlMTE5NSIsIm5hbWUiOiJUZXN0IEFwaSIsImVtYWlsIjoidGVzdEBhcGkuZnIiLCJpYXQiOjE2NDU3ODA0MTksImV4cCI6MTY0NTc4NDAxOX0.nEWh4o-tcBMPAKUThhp2A43hwCeNDKZKGDC-55NCv7s"
                }
            }
        },
        security: [
            {
                token: []
            }
        ]
    },
    apis: ["./app/routes/*.js"]
};


const specs = swaggerJsdoc(swaggerOptions);


app.use(
    "/docs",
    swaggerUi.serve,
    swaggerUi.setup(specs)
);

app.use([
    helmet(),
    cors(),
    jsonParser,
    morgan('combined'),
    errorsHandler,
    auth()
])


app.use('/api', router)


module.exports = app
