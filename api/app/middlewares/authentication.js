const expressJwt = require('express-jwt');
const secret = process.env.SECRET || 'secret'



const auth = (req, res, next) => {
    return expressJwt({ secret, algorithms: ['HS256'] }).unless({
        path: [
            '/api/login',
            '/api/logout'
        ]
    });
}

module.exports = {auth};