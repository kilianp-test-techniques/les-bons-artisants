const errorsHandler = (error, req, res, next) => {
    if (typeof error === 'string') {
        return res.status(400).json({ message: error })
    }

    if (error.name === 'UnauthorizedError') {
        return res.status(401).json({ message: 'Invalid Token' })
    }

    // default to 500 server error
    return res.status(500).json({ message: error.message })
}

module.exports = { errorsHandler }
