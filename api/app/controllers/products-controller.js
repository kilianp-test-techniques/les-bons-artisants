const { Products } = require('@@db')

const getProducts = async (req, res) => {
  try{
    const products = await Products.find()
    res.json(products)
  } catch(err){
    res.status(500).json({ message: err.message })
  }
}

const getProduct = async (req, res) => {
  try{
    const product = await Products.findById(req.params.id)
    if(product) {
      res.status(200).json(product)
    } else {
      res.status(404).json({
        message: 'Product not found'
      })
    }
  } catch(err) {
    res.status(500).json({
      message: err
    })
  }
}

const createProduct = async (req, res) => {
  try{
    const product = await Products.create(req.body)
    res.status(201).json(product)
  } catch(err) {
    res.status(400).json({
      message: err.message
    })
  }
}

const deleteProduct = async (req, res) => {
  try {
    const {id} = req.params
    const product = await Products.findByIdAndDelete(id)
    if(product) {
      res.status(200).json({
        message: 'Product deleted successfully'
      })
    } else {
      res.status(404).json({
        message: 'Product not found with id ' + id
      })
    }
  } catch(err) {
    res.status(500).json({
      message: 'Error deleting product',
      error: err
    })
  }
}

const updateProduct = async (req, res) => {
  try{
    const {name, type, price, rating, warranty_years, available} = req.body
    const {id} = req.params
    const product = await Products.findByIdAndUpdate(id, {
      name: name,
      type: type,
      price: price,
      rating: rating,
      warranty_years: warranty_years,
      available: available
    }, {
      new: true
    })
    if(product) {
      res.status(200).json(product)
    } else {
      res.status(404).json({
        message: 'Product not found'
      })
    }
  }catch(err){
    res.status(500).json({
      message: 'Error updating product'
    })
  }

}
module.exports = {
  getProducts,
  getProduct,
  createProduct,
  deleteProduct,
  updateProduct
}
