const {Users} = require('@@db')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const secret = process.env.SECRET || 'secret'

const getUsers = async (req, res) => {
    try{
        const users = await Users.find()
        if (users.length > 0) {
            users.forEach(user => {
                delete user._doc.password
            })
            console.log(users)
            res.status(200).json(users)
        } else {
            res.status(404).json({message: 'No users found'})
        }
    }catch(err){
        res.status(500).json({message: err.message})
    }
}

const getUser = async (req, res) => {
    try {
        const user = await Users.findById(req.params.id)
        if(!user) return res.status(404).json({message: 'User not found'})
        delete user._doc.password
        res.json(user)
    } catch (err) {
        res.status(500).json({message: err.message})
    }
}

const createUser = async (req, res) => {
    try {
        const {name, email, password} = req.body
        await bcrypt.hash(password, bcrypt.genSaltSync(10),null, async (err, hash) => {
            const user = new Users({
                name,
                email,
                password: hash
            })
            await user.save()
            res.status(201).json({
                message: 'User created successfully'
            })
        })
    }catch (err) {
        res.status(500).json({
            message: 'Error creating user',
            error: err
        })
    }
}

const deleteUser = async (req, res) => {
    try{
        const user = await Users.findById(req.params.id)
        if(!user) return res.status(404).json({message: 'User not found'})
        await user.remove()
        res.status(200).json({message: 'User deleted successfully'})
    }catch (err) {
        res.status(500).json({message: err.message})
    }
}

const updateUser = async (req, res) => {
    try{
        const user = await Users.findById(req.params.id)
        if(!user) return res.status(404).json({message: 'User not found'})
        const {name, email, password} = req.body
        await user.update({
            name,
            email,
            password
        })
        res.status(200).json({message: 'User updated successfully'})
    }catch (err) {
        res.status(500).json({message: err.message})
    }
}

const login = async (req, res) => {
    try{
        const {email, password} = req.body
        const user = await Users.findOne({
            where: {
                email
            }
        })
        if(!user) return res.status(404).json({message: 'User not found'})
        await bcrypt.compare(password, user.password, (err, isMatch) => {
            if(err) return res.status(500).json({message: err.message})
            if(!isMatch) return res.status(401).json({message: 'Invalid credentials'})
            const payload = {
                id: user._id,
                name: user.name,
                email: user.email
            }
            jwt.sign(payload, secret, {expiresIn: '1h'}, (err, token) => {
                if(err) return res.status(500).json({message: err.message})
                res.status(200).json({
                    _id: user._id,
                    name: user.name,
                    token
                })
            })
        })
    }catch (err) {
        res.status(500).json({message: err.message})
    }
}

const logout = async (req, res) => {
    try{
        const user = await Users.findById(req.body.id)
        if(!user) return res.status(404).json({message: 'User not found'})
        await user.update({
            token: null
        })
        res.status(200).json({message: 'Logout successful'})
    }catch (err) {
        res.status(500).json({message: err.message})
    }
}

module.exports = {
    getUsers,
    getUser,
    createUser,
    deleteUser,
    updateUser,
    login,
    logout
}

