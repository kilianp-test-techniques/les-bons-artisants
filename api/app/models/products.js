const pkg = require('mongoose')
const { model, Schema } = pkg


const Products = model('products', new Schema({
    name: {
        type: String,
        default: '',
    },
    type: {
        type: String,
        required: true,
    },

    price: {
        type: Number,
        required: true,
    },

    rating: {
        type: Number,
        required: true,
    },

    warranty_years: {
        type: Number,
        required: true,
    },

    available: {
        type: Number,
        required: true,
    },
}))

module.exports =  Products