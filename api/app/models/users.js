const pkg = require('mongoose')
const { model, Schema } = pkg


const Users = model('users', new Schema({
    name:{
        type: String,
        required: true,
    },
    email: {
        type: String,
        default: '',
    },
    password: {
        type: String,
        required: true,
    },
    token:{
        type: String,
        default: null,
    },
}))

module.exports =  Users