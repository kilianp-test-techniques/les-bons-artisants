import {CREATE_PRODUCT, DELETE_PRODUCT, EDIT_PRODUCT, GET_PRODUCTS} from "../actions/products-action";


const initialState = [];

export default function productsReducer(state = initialState, action) {
    switch (action.type) {
        case GET_PRODUCTS:
            return action.payload
        case EDIT_PRODUCT:
            return state.map(product => {
                if (product._id === action.payload._id) {
                    return action.payload;
                }
                return product;
            });
        case CREATE_PRODUCT:
            return [...state, action.payload];
        case DELETE_PRODUCT:
            return state.filter(product => product._id !== action.payload._id);

        default:
            return state;
    }
}