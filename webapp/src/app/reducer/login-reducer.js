import {AUTH_DISCONNECT, LOGIN, LOGIN_ERROR} from '../actions/login-action';


const initialState = localStorage.getItem('user') !== undefined ? localStorage.getItem('user') : null;

export default function loginReducer(state = initialState, action) {
    switch (action.type) {
        case AUTH_DISCONNECT:
            return {message: 'logout'};
        case LOGIN_ERROR:
            return {user: null, error: 'Login Failed'};
        case LOGIN:
            return action.payload;
        default:
            return state;
    }
}