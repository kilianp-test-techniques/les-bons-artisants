import React from 'react';
import Login from "../components/login/login-form";
import {useSelector} from "react-redux";
import {Fab, Icon} from "@mui/material";
import ProductContainer from "../components/product/product-container";
import {useDispatch} from "react-redux";
import {disconnect} from "../actions/login-action";

const HomePage = () => {

    const dispatch = useDispatch();
    let user = useSelector(state => state.user);

    if(localStorage.getItem('user')){
        user = JSON.parse(localStorage.getItem('user'));
    }

    const logout = () => {
        localStorage.removeItem('user');
        dispatch(disconnect(user));
    }

    const fabStyle = {
        position: 'fixed',
        top: '20px',
        right: '20px',
        zIndex: '1000'
    };

    if(user?.token){
        return (
            <div>
                <Fab onClick={logout} sx={fabStyle} variant="extended" size="medium" color="default"
                     aria-label="add">
                    <Icon sx={{mr: 1}}>logout</Icon>
                    {user.name}
                </Fab>
                <ProductContainer/>
            </div>
        );
    }else{
        return (
            <Login/>
        );
    }



       /* return (
            <div>
                <Fab onClick={logout} sx={logoutStyle} variant="extended" size="medium" color="default"
                     aria-label="add">
                    <Icon sx={{mr: 1}}>logout</Icon>
                    {user.name}
                </Fab>
                <ProductContainer/>
            </div>
        );*/

};


export default HomePage;