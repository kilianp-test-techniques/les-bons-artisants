import {login, logout} from "../services/login-api";

export const LOGIN = 'LOGIN';
export const AUTH_DISCONNECT = 'AUTH_DISCONNECT';
export const LOGIN_ERROR = 'LOGIN_ERROR';

export const loginUser = (data) => {
    return dispatch => {
        return login(data)
            .then(res => {
                console.log(res);
                localStorage.setItem('user', JSON.stringify(res));
                dispatch({
                    type: LOGIN,
                    payload: res
                });
            })
            .catch((err) => {
                throw err;
            });
    };
};

export const disconnect = (data) => {
    return dispatch => {
        return logout(data._id)
            .then(() => {
                dispatch({
                    type: AUTH_DISCONNECT,
                    payload: null
                });
            })
            .catch(err => {
                throw err;
            });
    };
};
