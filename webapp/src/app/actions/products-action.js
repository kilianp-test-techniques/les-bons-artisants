import {findAll, remove, save, update} from "../services/products-api";

export const GET_PRODUCTS = 'GET_PRODUCTS';
export const EDIT_PRODUCT = 'EDIT_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';

export const getProducts = () => {
    return dispatch => {
        return findAll()
            .then(res => {
                dispatch({
                    type: GET_PRODUCTS,
                    payload: res
                });
            })
            .catch(err => {
                throw err;
            });
    };
};

export const editProduct = (data) => {
    return dispatch => {
        return update(data).then(res => {
            dispatch({
                type: EDIT_PRODUCT,
                payload: res
            });
        })
            .catch(err => {
                throw err;
            });
    }
}

export const addProduct = (data) => {
    return dispatch => {
        return save(data).then(res => {
            dispatch({
                type: CREATE_PRODUCT,
                payload: res
            });
        })
            .catch(err => {
                throw err;
            });
    }
}

export const removeProduct = (data) => {
    return dispatch => {
        return remove(data._id).then(() => {
            dispatch({
                type: DELETE_PRODUCT,
                payload: data
            });
        })
            .catch(err => {
                throw err;
            });
    }
}