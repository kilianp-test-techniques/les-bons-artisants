import {configureStore} from '@reduxjs/toolkit'

import productsReducer from "./reducer/products-reducer";
import loginReducer from "./reducer/login-reducer";

const store = configureStore({
    reducer: {
        products: productsReducer,
        user: loginReducer
    }
});

export default store