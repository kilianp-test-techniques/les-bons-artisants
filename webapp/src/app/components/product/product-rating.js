import React from 'react';
import {Box, Rating} from "@mui/material";

const ProductRating = (row) => {
    let product = row.product
    return (
        <Box
            sx={{
                width: 10,
                display: 'flex',
                alignItems: 'flex-end',
            }}
        >
            <Rating name="read-only" value={product.rating} readOnly
                 />
            <Box sx={{ml: 2}}>{product.rating}/5</Box>
        </Box>
    );
};

export default ProductRating;