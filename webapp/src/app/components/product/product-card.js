import React from 'react';
import ProductRating from "./product-rating";
import ProductAvailability from "./product-availability";
import EditModal from "../modals/edit-modal";
import DeleteModal from "../modals/delete-modal";
import {Card, CardContent, Chip, Stack, Typography} from "@mui/material";

const ProductCard = (row) => {

    const product = row.product;

    return (
        <>
            <Card sx={{my: 2, maxWidth: 800, p: 1}}>
                <CardContent>
                    <Stack spacing={1} direction={"row"} justifyContent={"space-between"}>
                        <Typography variant="h5" component="h2">
                            {product.name}
                        </Typography>
                        <Typography variant="h5" color="text">
                            {product.price + ' €'}
                        </Typography>
                    </Stack>
                    <Typography variant="body" color="text.secondary">
                        <ProductRating product={product}/>
                    </Typography>


                </CardContent>
                <Stack spacing={1} direction={"row"} justifyContent={"space-between"}>
                    <Stack sx={{width:280}} spacing={1} direction={"row"} alignItems={"center"} justifyContent={"space-between"} >
                         <Chip sx={{px: 2, m: 10}}
                              label={product.warranty_years + ' year' + (product.warranty_years > 1 ? 's' : '' )+ ' warranty'}/>
                        <ProductAvailability product={product}/>
                    </Stack>
                    <Stack sx={{width:380}} spacing={1} direction={"row"} justifyContent={"flex-end"} >
                        <EditModal product={product}/>
                        <DeleteModal product={product}/>
                    </Stack>
                </Stack>

            </Card>
        </>
    );
};

export default ProductCard;