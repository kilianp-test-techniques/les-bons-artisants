import React from 'react';
import {Chip, Icon} from "@mui/material";

const ProductAvailability = (row) => {
    let product = row.product;
    return (
        <div>
            {product.available ?

                <Chip color={"success"} sx={{width:130}} icon={ <Icon>check_circle</Icon>} label="Available" />

                :
                <Chip color={"error"} sx={{width:130}} icon={ <Icon>cancel_circle</Icon>} label="Not Available" />
            }
        </div>
    );
};

export default ProductAvailability;