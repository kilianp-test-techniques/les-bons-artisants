import React, {useEffect} from "react";
import ProductCard from "./product-card";
import NewProductModal from "../modals/new-product-modal";
import {Stack} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {getProducts} from "../../actions/products-action";

export default function ProductContainer() {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getProducts());
    }, [dispatch]);

    const groupBy = (xs, key) => {
        return xs.reduce((rv, x) => {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    };
    const category = groupBy(useSelector(state => state.products), 'type');
    console.log(Object.keys(category));
if (Object.keys(category).length === 0) return <> <Stack alignItems={'center'}>
    <NewProductModal/>
    <h1>No Products</h1>
</Stack></>
else {
    return (
        <>
            <NewProductModal/>
            <Stack alignItems={'center'}>
                {Object.keys(category).map((key, index) => {
                    return (
                        <Stack key={index}>
                            <h2><b>Category : </b>{key}</h2>
                            {category[key].map((product, index) => {
                                return (
                                    <ProductCard key={index} product={product}/>
                                )
                            })}
                        </Stack>
                    )
                })}
            </Stack>
        </>
    );
}
}
