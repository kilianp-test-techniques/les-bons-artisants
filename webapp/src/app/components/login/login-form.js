import React from 'react';
import store from "../../store";
import {loginUser} from "../../actions/login-action";
import {Box, Button, Container, TextField, Typography} from "@mui/material";
import {useSnackbar} from "notistack";

export default function LoginForm() {
    const {enqueueSnackbar} = useSnackbar();
    const dispatch = store.dispatch;

    const handleSubmit = async e => {
        e.preventDefault();
        const data = new FormData(e.currentTarget);
        dispatch(loginUser({
            email: data.get('email'),
            password: data.get('password'),
        })).then(()=>{
            enqueueSnackbar('Login Successful', {
                persist: false,
                autoHideDuration: 3000,
                variant: 'success',
                anchorOrigin: {
                    vertical: 'top',
                    horizontal: 'center',
                },
            })
        }).catch(()=>{
            enqueueSnackbar('Login Failed, Check Email & Password', {
                persist: false,
                autoHideDuration: 3000,
                variant: 'error',
                anchorOrigin: {
                    vertical: 'top',
                    horizontal: 'center',
                },
            })
        });
    }

    return (
        <Container component="main" maxWidth="xs">
            <Box
                sx={{
                    marginTop: 14,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <Box component="form" onSubmit={handleSubmit} noValidate sx={{mt: 1}}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        placeholder={'test@api.fr'}
                        autoComplete="email"
                        autoFocus
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        placeholder={'pswd'}
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Sign In
                    </Button>
                </Box>
            </Box>
        </Container>
    )
}