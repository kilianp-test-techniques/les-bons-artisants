import React, {useState} from 'react';
import {
    Box,
    Button,
    Checkbox,
    FormControlLabel,
    Icon,
    IconButton,
    InputAdornment,
    Modal,
    Rating,
    Stack,
    TextField
} from "@mui/material";
import {editProduct} from "../../actions/products-action";
import store from "../../store";
import {useSnackbar} from "notistack";

const EditModal = (row) => {
    const {enqueueSnackbar} = useSnackbar();

    const product = row.product;
    const dispatch = store.dispatch

    const [open, setOpen] = useState(false);
    const [rating, setRating] = useState(product.rating);
    const [name, setName] = useState(product.name);
    const [price, setPrice] = useState(product.price);
    const [type, setType] = useState(product.type);
    const [available, setAvailable] = useState(!!product.available);
    const [warranty, setWarranty] = useState(product.warranty_years);

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        bgcolor: 'background.paper',
        p: 4
    };

    const saveProduct = async () => {

        let newProduct = {
            id: product._id,
            name: name,
            price: price,
            type: type,
            available: available,
            rating: rating,
            warranty_years: warranty
        };
        dispatch(editProduct(newProduct)).then(() => {
            enqueueSnackbar('Product updated', {variant: 'success',autoHideDuration: 3000});
        }).catch(() => {
            enqueueSnackbar('Error updating product', {variant: 'error',autoHideDuration: 3000});
        });
        handleClose();
    };

    return (
        <div>
            <IconButton onClick={handleOpen}><Icon>edit_circle</Icon></IconButton>
            <Modal
                open={open}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <form onSubmit={saveProduct}>
                    <TextField
                        fullWidth
                        required
                        margin={"dense"}
                        id="outlined-basic"
                        label="Name"
                        variant="outlined"
                        value={name}
                        onChange={(e) => {
                            setName(e.target.value);
                        }}/>
                    <TextField
                        fullWidth
                        required
                        margin={"dense"}
                        id="outlined-basic"
                        label="Type"
                        variant="outlined"
                        value={type}
                        onChange={(e) => {
                            setType(e.target.value);
                        }}/>
                    <TextField
                        fullWidth
                        required
                        type={'number'}
                        margin={"dense"}
                        id="outlined-basic"
                        label="Price"
                        InputProps={{
                            endAdornment: <InputAdornment position="end">€</InputAdornment>,
                        }}
                        variant="outlined"
                        value={price}
                        onChange={(e) => {
                            setPrice(e.target.value);
                        }}/>
                    <Box sx={{
                        display: "flex",
                        alignItems: "center"
                    }}>
                        <Rating name="rating" value={rating}
                                margin={"dense"}
                                precision={0.1}
                                onChange={(event, newValue) => {
                                    setRating(newValue);
                                }}/>{rating}
                    </Box>
                    <TextField
                        fullWidth
                        required
                        margin={"dense"}
                        type={"number"}
                        id="outlined-basic"
                        label="Warranty"
                        InputProps={{
                            endAdornment: <InputAdornment position="end">years</InputAdornment>,
                        }}
                        variant="outlined"
                        value={warranty}
                        onChange={(e) => {
                            setWarranty(e.target.value);
                        }}/>
                    <FormControlLabel
                        label="Available"
                        control={
                            <Checkbox
                                lang={"Available"}
                                checked={available}
                                onChange={(e,newValue) => {
                                    setAvailable(newValue);
                                }}
                                inputProps={{'aria-label': 'controlled'}}
                            />}
                    />
                    <Stack spacing={2} direction="row">
                        <Button variant="outlined" onClick={handleClose}>Cancel</Button>
                        <Button variant="contained" type={"submit"}>Save</Button>
                    </Stack>
                    </form>
                </Box>
            </Modal>
        </div>
    );
};

export default EditModal;