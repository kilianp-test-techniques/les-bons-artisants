import React, {useState} from 'react';
import {Box, Button, Icon, IconButton, Modal, Stack} from "@mui/material";
import store from "../../store";

import {useSnackbar} from 'notistack';
import {removeProduct} from "../../actions/products-action";

const DeleteModal = ({product}) => {
    const {enqueueSnackbar} = useSnackbar();

    const [open, setOpen] = useState(false);

    const handleOpen = () => {setOpen(true);};
    const handleClose = () => {setOpen(false);};

    const dispatch = store.dispatch;

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        bgcolor: 'background.paper',
        p: 4

    };



    const deleteProduct = () => {
        dispatch(removeProduct(product)).then(() => {
            enqueueSnackbar('Product deleted', {variant: 'success',autoHideDuration: 3000});
        }).catch(() => {
            enqueueSnackbar('Error deleting product', {variant: 'error',autoHideDuration: 3000});
        });
        handleClose();
    };


    return (
        <div>
            <IconButton onClick={handleOpen}><Icon>delete_circle</Icon></IconButton>
            <Modal
                open={open}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    Voulez-vous supprimer le produit : <b>{product.name}</b>
                <Stack spacing={0} direction="row" alignItems={"center"} justifyContent={"center"}>
                    <Button>Annuler</Button>
                    <Button onClick={deleteProduct}>Supprimer</Button>
                </Stack>
                </Box>
            </Modal>
        </div>
    );
};

export default DeleteModal;