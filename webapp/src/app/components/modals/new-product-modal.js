import React, {useState} from 'react';
import {
    Box,
    Button,
    Checkbox,
    Fab,
    FormControlLabel,
    Icon,
    InputAdornment,
    Modal,
    Rating,
    Stack,
    TextField
} from "@mui/material";
import {addProduct} from "../../actions/products-action";
import store from "../../store";
import {useSnackbar} from "notistack";

const NewProductModal = () => {

    const [open, setOpen] = useState(false);
    const [rating, setRating] = useState(0);
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [type, setType] = useState('');
    const [available, setAvailable] = useState(false);
    const [warranty, setWarranty] = useState(0);

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const {enqueueSnackbar} = useSnackbar();

    const dispatch = store.dispatch

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        bgcolor: 'background.paper',
        p: 4

    };

    const fabStyle = {
        position: 'fixed',
        bottom: 16,
        right: 16,
    };

    const reset = () => {
        setRating(0);
        setName('');
        setPrice('');
        setType('');
        setAvailable(false);
        setWarranty(0);
    };

    const saveProduct = async () => {

        let newProduct = {
            name: name,
            price: price,
            type: type,
            available: available,
            rating: rating,
            warranty_years: warranty
        };
        dispatch(addProduct(newProduct)).then(() => {
            enqueueSnackbar('Product added', {variant: 'success',autoHideDuration: 3000});
        }).catch(() => {
            enqueueSnackbar('Error adding product', {variant: 'error',autoHideDuration: 3000});
        });
        reset();
        handleClose();
    };

    return (
        <div>
            <Fab onClick={handleOpen} sx={fabStyle} variant="extended" size="medium" color="primary" aria-label="add">
                <Icon sx={{mr: 1}}>add</Icon>
                New Product
            </Fab>
            <Modal
                open={open}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <form onSubmit={saveProduct}>
                        <TextField
                            fullWidth
                            margin={"dense"}
                            id="outlined-basic"
                            label="Name"
                            variant="outlined"
                            value={name}
                            required
                            onChange={(e) => {
                                setName(e.target.value);
                            }}/>
                        <TextField
                            fullWidth
                            margin={"dense"}
                            id="outlined-basic"
                            label="Type"
                            variant="outlined"
                            required
                            value={type}
                            onChange={(e) => {
                                setType(e.target.value);
                            }}/>
                        <TextField
                            fullWidth
                            margin={"dense"}
                            type={'number'}
                            id="outlined-basic"
                            label="Price"
                            required
                            InputProps={{
                                endAdornment: <InputAdornment position="end">€</InputAdornment>,
                            }}
                            variant="outlined"
                            value={price}
                            onChange={(e) => {
                                setPrice(e.target.value);
                            }}/>
                        <Box sx={{
                            display: "flex",
                            alignItems: "center"
                        }}>
                            <Rating name="rating" value={rating}
                                    margin={"dense"}
                                    precision={0.1}
                                    onChange={(event, newValue) => {
                                        setRating(newValue);
                                    }}/>{rating}
                        </Box>
                        <TextField
                            fullWidth
                            margin={"dense"}
                            type={"number"}
                            required
                            id="outlined-basic"
                            label="Warranty"
                            InputProps={{
                                endAdornment: <InputAdornment position="end">years</InputAdornment>,
                            }}
                            variant="outlined"
                            value={warranty}
                            onChange={(e) => {
                                setWarranty(e.target.value);
                            }}/>
                        <FormControlLabel
                            label="Available"
                            required
                            control={
                                <Checkbox
                                    lang={"Available"}
                                    checked={available}
                                    onChange={(e,newValue) => {
                                        setAvailable(newValue);
                                    }}
                                    inputProps={{'aria-label': 'controlled'}}
                                />}
                        />
                        <Stack spacing={2} direction="row">
                            <Button variant="outlined" onClick={handleClose}>Cancel</Button>
                            <Button variant="contained" type="submit">Save</Button>
                        </Stack>
                    </form>
                </Box>
            </Modal>
        </div>
    );
};

export default NewProductModal;