import axios from "axios";
import {PRODUCTS_API} from "../../config";

const api = axios.create({
    baseURL: PRODUCTS_API,
});

api.interceptors.request.use(
    config => {
        const user = JSON.parse(localStorage.getItem("user")) || {};

        if (user?.token) {
            config.headers.Authorization = `Bearer ${user.token}`
        }
        return config
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    }
);



export async function findAll() {
    const response = await api.get('/');
    return response.data
}

export async function find(id) {
    const response = await api.get('/' + id);
    return response.data
}

export async function save(product) {
    const response = await api.post('/', product);
    return response.data
}

export async function update(product) {
    const response = await api.put('/' + product.id, product);
    return response.data
}

export async function remove(id) {
    await api.delete('/' + id);
}