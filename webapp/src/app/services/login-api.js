import axios from "axios";

import {API_URL} from "../../config";

export async function login(credentials) {
    const response = await axios.post(API_URL + 'login',credentials);
    return response.data
}

export async function logout(userId){
    const response = await axios.post(API_URL + 'logout', {id:userId});
    return response.data
}