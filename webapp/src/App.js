import './App.css';
import React from 'react';
import HomePage from "./app/pages/home-page";
import store from "./app/store";
import {Provider} from "react-redux";
import {SnackbarProvider} from "notistack";

function App() {

    return (
        <>
            <SnackbarProvider maxSnack={3}>
            <Provider store={store}>
                <HomePage/>
            </Provider>
            </SnackbarProvider>
        </>
  );
}

export default App;
