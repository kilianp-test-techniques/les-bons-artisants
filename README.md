# Les Bons Artisants - Test Technique

> By Kilian PICHARD
>
Estimated spent time: 21 hours

- [x] Rest API in NodeJS/Express with MongoDB
- [x] ReactJS web app to display the data + CRUD - Material UI

  >Bonus
- [x] JWT authentication
- [x] Redux for state management
- [x] ESlint for code quality

# Not Done

- [ ] Websocket to update the data in real time

# Requirements

- NodeJS (v16.14.0)
- Docker
- NPM (v8.3.1)

# Architecture

NodeJS API

```
├───app
│   ├─── controllers            # Controllers for Product, User
│   ├─── middlewares            # Authentication with JWT and Error handling
│   ├─── models                 # MongoDB models for Product, User
│   ├─── routes                 # Routes for Product, User
|   └─── index.js               # Main file
└─── server.js                  # Server file
```
ReactJS Frontend

```
├─── app
│     ├─── actions        # Redux actions
│     ├─── components     # Components for Products display, Modals and Login Page
│     ├─── pages          # Home page Template
│     ├─── reducer        # Redux reducer
│     ├─── services       # Api services
│     └─── store.js       # Redux store
├─── App.js               # App File
└─── index.js             # Main file
```
# Documentation
You can find a basic documentation of the REST API here:
```
http://localhost:3001/docs
```
# Running the application
## Run containers

> Requires docker to be installed
```
docker-compose up
```


# Access the Application
```
http://localhost:3000/

Login : test@api.fr
Password : pswd
```
